NOTICE: Backend API is currently down after a year of hosting, and I (Grant Martin) do not have access to the old database. Working on repopulating and rehosting a new database!

Website: https://www.concertfor.me/

Presentation: https://youtu.be/FPmCq0AX_8c 

Group Members (UT EID, Gitlab ID): 
- Jackson Nakos: jn27746, jacksonnakos
- Minh Nguyen: nn7294, nhminhng
- Divya Kalanee: dk27966, divyakalanee
- Timothy Zhang: tz3723, zhang.timothy
- Grant Martin: glm2367, grantmartin2002



