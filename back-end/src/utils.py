import psycopg2
import constants
import os
def get_conn():
    conn = psycopg2.connect(
        database=constants.PG_DB_NAME,
        user=constants.PG_DB_USER,
        password=constants.PG_DB_PASSWORD,
        host=constants.PG_DB_HOST,
        port=constants.PG_DB_PORT,
    )
    return conn

def execute_sql_file(filepath, conn=None):
    print(os.getcwd())
    if conn==None:
        conn = get_conn()
    try:
        conn = get_conn()
        cur = conn.cursor()

        with open(filepath, 'r') as file:
            sql_script = file.read()
        
        # Split the script into commands and execute each command
        # Note: This simplistic splitting may not work for all SQL scripts, especially those containing semicolons within strings or comments.
        commands = sql_script.split(';')
        for command in filter(None, commands):  # Filter out empty commands
            try:
                cur.execute(command)
            except (Exception, psycopg2.DatabaseError) as error:
                print(f"Command {command} resulted in this Error: {error}")
            
        conn.commit()  # Commit the transactions to the database
    except (Exception, psycopg2.DatabaseError) as error:
        print(f"Error: {error}")
    finally:
        if cur is not None:
            cur.close()
        if conn is not None:
            conn.close()