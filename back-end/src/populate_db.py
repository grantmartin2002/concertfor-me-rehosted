import psycopg2
from songs_api import get_songs
from psycopg2.extensions import AsIs
import utils


def get_field(dict, s):
    try:
        return dict[s]
    except:
        return "Not Found"


def get_url(dict, s):
    try:
        return dict[s]
    except:
        return (
            "https://upload.wikimedia.org/wikipedia/commons/a/ac/No_image_available.svg"
        )


def get_int(dict, s):
    try:
        return str(dict[s])
    except:
        return "0"


def main():
    conn = utils.get_conn()
    cursor = conn.cursor()

    # Populate acj table
    s = """
    SELECT art.artist_id, con.concert_id, art.artist_name, con.event_name
    FROM artists art JOIN concerts con
    ON art.artist_name = con.artist_name;
  """
    cursor.execute(s)
    data = cursor.fetchall()
    acjid = 1
    for d in data:
        q = (
            "INSERT INTO artist_concert_junction (acj_id, artist_id, concert_id, \
        artist_name, concert_name) VALUES (\
          "
            + str(acjid)
            + ", "
            + str(d[0])
            + "\
        , "
            + str(d[1])
            + ", '"
            + str(d[2])
            + "', '"
            + str(d[3])
            + "');"
        )
        cursor.execute(q)
        acjid += 1

    s = "SELECT artist_id, artist_name FROM artists;"
    cursor.execute(s)
    data = cursor.fetchall()
    sid = 1
    asjid = 1
    # d will be each dict from artist
    for d in data:
        rows = get_songs(d[1])

        for r in rows["results"]:
            for a in r:
                if isinstance(r[a], str):
                    for l in r[a]:
                        if l == "'":
                            r[a] = r[a].replace(l, '"')

            # Add statement to check if song already exists
            s = (
                "SELECT 1 AS r FROM songs WHERE song_name = \
      '"
                + r["trackName"]
                + "' LIMIT(1);"
            )
            cursor.execute(s)
            st = cursor.fetchall()
            if st != []:
                continue

            s = (
                "SELECT 1 AS r FROM artists WHERE artist_name = \
      '"
                + r["artistName"]
                + "' LIMIT(1);"
            )
            cursor.execute(s)
            st = cursor.fetchall()
            if st == []:
                continue

            # Add to songs, add to songs junction
            """
      song = {'song_id' : sid,
              'artists' : r['artistName'],
              'album'   : r['collectionName'],
              'song_name': r['trackName'],
              'artist_view_url' : r['artistViewUrl'],
              'track_view_url' : r['trackViewUrl'],
              'preview_url' : r['previewUrl'],
              'artwork_url' : r['artworkUrl100'],
              'price' : r['trackPrice'],
              'release_date' : r['releaseDate'],
              'explicit' : r['trackExplicitness'],
              'track_count' : r['trackCount'],
              'track_number' : r['trackNumber'],
              'track_length' : r['trackTimeMillis'],
              'country' : r['country'],
              'currency' : r['currency'],
              'genre' : r['primaryGenreName']}
      """
            try:
                q = (
                    "INSERT INTO songs (song_id, artists, album, song_name,\
        artist_view_url, track_view_url, preview_url, artwork_url, price,\
        release_date, explicit, track_count, track_number,\
        track_length, country, currency, genre) VALUES \
        ("
                    + str(sid)
                    + ", \
        '"
                    + get_field(r, "artistName")
                    + "', \
        '"
                    + get_field(r, "collectionName")
                    + "', \
        '"
                    + get_field(r, "trackName")
                    + "', \
        '"
                    + get_url(r, "artistViewUrl")
                    + "', \
        '"
                    + get_url(r, "trackViewUrl")
                    + "', \
        '"
                    + get_url(r, "previewUrl")
                    + "', \
        '"
                    + get_url(r, "artworkUrl100")
                    + "', \
        "
                    + get_int(r, "trackPrice")
                    + ", \
        '"
                    + get_field(r, "releaseDate")
                    + "', \
        '"
                    + get_field(r, "trackExplicitness")
                    + "', \
        "
                    + get_int(r, "trackCount")
                    + ", \
        "
                    + get_int(r, "trackNumber")
                    + ", \
        "
                    + get_int(r, "trackTimeMillis")
                    + ", \
        '"
                    + get_field(r, "country")
                    + "', \
        '"
                    + get_field(r, "currency")
                    + "', \
        '"
                    + get_field(r, "primaryGenreName")
                    + "' \
        );"
                )
                cursor.execute(q)
            except:
                print(r)
                assert False

            # Now songs table has row, add to the junction
            # Multiple artists are separated by & symbol

            arts = r["artistName"].split(" & ")
            for a in arts:
                q = (
                    "INSERT INTO artist_song_junction (asj_id, artist_id, song_id, \
        artist_name, song_name) VALUES ("
                    + str(asjid)
                    + ", "
                    + str(d[0])
                    + "\
        , "
                    + str(sid)
                    + ", '"
                    + a
                    + "', '"
                    + r["trackName"]
                    + "');"
                )
                cursor.execute(q)
                asjid += 1

            sid += 1

    # Assume ACJ and ASJ are filled in
    s = """
    SELECT acj.concert_id, asj.song_id, acj.concert_name, asj.song_name
    FROM artist_concert_junction acj JOIN
    artist_song_junction asj ON acj.artist_id = asj.artist_id;
  """
    cursor.execute(s)
    data = cursor.fetchall()
    csjid = 1
    for d in data:
        q = (
            "INSERT INTO concert_song_junction (csj_id, concert_id, song_id, \
        concert_name, song_name) VALUES ("
            + str(csjid)
            + ", "
            + str(d[0])
            + "\
        , "
            + str(d[1])
            + ", '"
            + str(d[2])
            + "', '"
            + str(d[3])
            + "');"
        )
        cursor.execute(q)
        csjid += 1

    conn.commit()
    # Closing the connection
    cursor.close()
    conn.close()


if __name__ == "__main__":
    main()
