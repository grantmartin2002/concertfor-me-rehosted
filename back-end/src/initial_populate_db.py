import utils
import artists_api
import concerts_api
import songs_api
import json

ARTIST_INSERT_QUERY = """
    INSERT INTO artists (artist_name, spotify_page_url, followers, popularity, genre, spotify_id, image_url, birthday, aliases, gender, home_country) 
    VALUES (%(artist_name)s, %(spotify_page_url)s, %(followers)s, %(popularity)s, %(genre)s, %(spotify_id)s, %(image_url)s, %(birthday)s, %(aliases)s, %(gender)s, %(home_country)s);
    """
SONG_INSERT_QUERY = """"""
CONCERT_INSERT_QUERY = """"""

conn = utils.get_conn()
cur = conn.cursor()
def create_tables():
    utils.execute_sql_file("back-end/scripts/create_tables.sql", conn)
    

def delete_tables():
    cur.execute("DROP TABLE artists CASCADE")
    cur.execute("DROP TABLE concerts CASCADE")
    cur.execute("DROP TABLE songs CASCADE")
    cur.execute("DROP TABLE concert_song_junction CASCADE")
    cur.execute("DROP TABLE artist_song_junction CASCADE")
    cur.execute("DROP TABLE artist_concert_junction CASCADE")
    conn.commit()


def add_artist(artist_dict):
    cur.execute(ARTIST_INSERT_QUERY, artist_dict)
    conn.commit()

def add_artists(artist_dicts):
    cur.executemany(ARTIST_INSERT_QUERY, artist_dicts)
    conn.commit()

def add_song(song_dict):
    cur.execute(SONG_INSERT_QUERY, song_dict)
    conn.commit()

def add_songs(song_dicts):
    cur.execute(SONG_INSERT_QUERY, song_dicts)
    conn.commit()

def add_concert(concert_dict):
    cur.execute(CONCERT_INSERT_QUERY, concert_dict)
    conn.commit()

def add_songs(concert_dict):
    cur.execute(CONCERT_INSERT_QUERY, concert_dict)
    conn.commit()

delete_tables()
print("Deleted tables!")
create_tables()
print("Created tables!")
# get top 100 artists
artist_data = json.loads(artists_api.get_artists(1))
print("got artist data!")
add_artists(artist_data['artists'])
print("added artist data to database!")
# get songs for all the artists
# insert the concerts for the artists

# insert the artists songs





