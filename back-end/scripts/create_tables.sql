CREATE TABLE IF NOT EXISTS artists (
    artist_id SERIAL PRIMARY KEY,
    artist_name TEXT,
    spotify_page_url TEXT,
    followers INT,
    popularity INT,
    genre TEXT,
    spotify_id TEXT,
    birthday TEXT,
    aliases TEXT,
    image_url TEXT,
    gender TEXT,
    home_country TEXT
);

CREATE TABLE IF NOT EXISTS concerts (
    concert_id SERIAL PRIMARY KEY,
    event_name TEXT,
    event_description TEXT,
    venue_name TEXT,
    venue_address TEXT,
    predicted_attendance INT,
    start_date TEXT,
    start_time TEXT,
    timezone TEXT,
    artist_name TEXT,
    image_url TEXT
);

CREATE TABLE IF NOT EXISTS songs (
    song_id SERIAL PRIMARY KEY,
    artist TEXT,
    album TEXT,
    song_name TEXT,
    artist_view_url TEXT,
    track_view_url TEXT,
    preview_url TEXT,
    artwork_url TEXT,
    price MONEY,
    release_date TEXT,
    explicit TEXT,
    track_number INT,
    track_count INT,
    track_length INT,
    country TEXT,
    currency TEXT,
    genre TEXT
);

CREATE TABLE IF NOT EXISTS concert_song_junction (
    csj_id SERIAL PRIMARY KEY,
    concert_id INT,
    song_id INT,
    concert_name TEXT,
    song_name TEXT,
    FOREIGN KEY (concert_id) REFERENCES concerts(concert_id) ON DELETE CASCADE,
    FOREIGN KEY (song_id) REFERENCES songs(song_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS artist_song_junction (
    asj_id SERIAL PRIMARY KEY,
    artist_id INT,
    song_id INT,
    artist_name TEXT,
    song_name TEXT,
    FOREIGN KEY (artist_id) REFERENCES artists(artist_id) ON DELETE CASCADE,
    FOREIGN KEY (song_id) REFERENCES songs(song_id) ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS artist_concert_junction (
    acj_id SERIAL PRIMARY KEY,
    artist_id INT,
    concert_id INT,
    artist_name TEXT,
    concert_name TEXT,
    FOREIGN KEY (artist_id) REFERENCES artists(artist_id) ON DELETE CASCADE,
    FOREIGN KEY (concert_id) REFERENCES concerts(concert_id) ON DELETE CASCADE
);
