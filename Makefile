build:
	cd front-end && 	npm install

run:
	cd front-end && npm start

test_jest_frontend:
	cd front-end && npm test

test_selenium:
	# cd ./front-end/gui-tests/ && chmod +rwx chromedriver_linux
	python3 front-end/gui-tests/selenium_test.py


