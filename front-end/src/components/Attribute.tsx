
const Attribute = (props: any) => {
    let attributeName = props.name
    let attributeValue = props.value
    return (
        <>
            <b>{attributeName}: </b>
            <>
                {attributeValue}
            </>
            <br></br>
        </>
    )
}
export default Attribute