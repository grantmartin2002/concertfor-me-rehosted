import React from 'react'
import Container from 'react-bootstrap/Container';
import Card from 'react-bootstrap/Card';

const APICard = (props: any) => {
    let name = props.name
    let use = props.use
    let howItWasScraped = props.howItWasScraped
    let photo = props.photo
    let link = props.link
    return (
        <Container style={{ width: "100%"}}>
            <div>
                <Card style={{ width: '18rem' }}>
                    <Card.Body>
                        <Card.Img variant="top" src={photo}/>
                        <Card.Title>{name}</Card.Title>
                        <Card.Text>
                            Use: {use} <br></br>
                            HowItWasScraped:{howItWasScraped} <br></br>
                        </Card.Text>
                        <Card.Link href={link}>Link To API</Card.Link>
                    </Card.Body>

                </Card>

            </div>
        </Container>
    )
}

export default APICard